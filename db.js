const dbConfig = require("./app-config").getDbConfig()
const Sequelize = require('sequelize')
const { host, database, username, password } = dbConfig

let instance

module.exports = (function () {
  
  function createInstance() {
      return new Sequelize(database, username, password, {
        host: host,
        dialect: 'mssql',
        logging: false,
        pool: {
          max: 5,
          min: 0,
          acquire: 30000,
          idle: 10000
        }
      })
  }

  return {
      getInstance: function () {
          if (!instance) {
              instance = createInstance()
          }
          return instance
      }
  }
})()




// export default class Singleton{  
//     constructor() {
//         if(!instance){
//               instance = this;
//         }

//         this.sequelize = new Sequelize(database, username, password, {
//           host: host,
//           dialect: 'mssql',
//           pool: {
//             max: 5,
//             min: 0,
//             acquire: 30000,
//             idle: 10000
//           }
//         })

//         return instance;
//       }
// }