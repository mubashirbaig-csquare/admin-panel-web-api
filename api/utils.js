const crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq',
    nodemailer = require('nodemailer'),
    appConfig = require('../app-config')

exports.encrypt = function (text) {
    const cipher = crypto.createCipher(algorithm, password)
    let crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

exports.decrypt = function (text) {
    const decipher = crypto.createDecipher(algorithm, password)
    let dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}

exports.sendEmail = function (password, toEmail, callback) {
    let transporter = nodemailer.createTransport({
        host: appConfig.EMAIL_HOST,
        port: appConfig.EMAIL_PORT,
        secure: appConfig.EMAIL_SECURE, // true for 465, false for other ports
        auth: {
            user: appConfig.EMAIL_USERNAME,
            pass: appConfig.EMAIL_PASSWORD
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: appConfig.EMAIL_FROM_ACCOUNT, // sender address
        to: toEmail,//'mubashir.baig@outlook.com', // list of receivers
        subject: appConfig.EMAIL_SUBJECT, // Subject line
        text: `         Dear User, \n        
        Welcome to KIOSK Admin Panel \r
        
        Please note the following information \r
        _____________________________________ \r\n
        User ID: ${toEmail}\r
        Password: ${password}\r
        \r
        If you have trouble in login, Please email at abc@abc.com \r\n
        Thank You \r\n
        KIOSK Admin Panel \r\n`, // plain text body
        // html: '<b>Hello world?</b>' // html body
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
            callback(error)
        } else {
            callback(null)
        }
    });
}

exports.doInfoLog = function (fileName, reqPathUrl, descriptionForLogData, dataToLog) {
    const log = require('../log-config').getLogger(fileName)
    log.info(`req url: ${reqPathUrl} \n  ${descriptionForLogData} ${JSON.stringify(dataToLog)}`)
}

exports.doErrorLog = function (fileName, reqPathUrl, descriptionForLogData, dataToLog) {
    const log = require('../log-config').getLogger(fileName)
    console.log(descriptionForLogData)
    log.error(`req url: ${reqPathUrl} \n  ${descriptionForLogData} ${JSON.stringify(dataToLog)}`)
}

exports.doFatalLog = function (fileName, reqPathUrl, descriptionForLogData, dataToLog) {
    const log = require('../log-config').getLogger(fileName)
    console.log(descriptionForLogData)
    log.fatal(`req url: ${reqPathUrl} \n  ${descriptionForLogData} ${JSON.stringify(dataToLog)}`)
}
