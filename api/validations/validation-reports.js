const Joi = require('joi');

module.exports.fromDateAndToDateParamsCheck = Joi.object().keys({
    fromDate: Joi.string().required(),
    toDate: Joi.string().required()
}).with('fromDate', 'toDate')