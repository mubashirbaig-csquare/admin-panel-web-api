const Joi = require('joi')


module.exports.verifyUserCredentials = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().alphanum().min(6).max(10).required()
}).with('email', 'password')