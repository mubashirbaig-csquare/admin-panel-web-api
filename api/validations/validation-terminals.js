const Joi = require('joi');

// module.exports = Joi.object().keys({
//     email: Joi.string().email().required(),
//     password: Joi.string().alphanum().min(6).max(10).required()
// }).with('email', 'password')

module.exports.addNewTerminalWithDevices = Joi.object().keys({
    terminalName: Joi.string().required(),
    ipAddress: Joi.string().required(),
    macAddress: Joi.string().required(),
    keepAliveInterval: Joi.string().required(),
    branchId: Joi.string().required(),
    licenseId: Joi.string().required(),
    licenseKey: Joi.string().required(),
    userId: Joi.string().required()
})


module.exports.updateTerminal = Joi.object().keys({
    terminalName: Joi.string().required(),
    ipAddress: Joi.string().required(),
    macAddress: Joi.string().required(),
    keepAliveInterval: Joi.string().required(),
    branchId: Joi.string().required(),
    licenseId: Joi.string().required(),
    status: Joi.string().required(),
    userId: Joi.string().required(),
    statusChanged: Joi.string().required(),
    reasonForStatus: Joi.string().required(),
    terminalStatus: Joi.string().required()
})


module.exports.updateTerminalCriticalStatus = Joi.object().keys({
    userId: Joi.string().required(),
    comments: Joi.string().required()
}).with('userId', 'comments')

module.exports.updateSerialNumberOfSpecificDeviceOfATerminal  = {
    serialNumber: Joi.string().required()
}

// module.exports.updateTerminalCriticalStatus = Joi.object().keys({
//     userId: Joi.string().required(),
//     comments: Joi.string().required()
// }).with('userId', 'comments')