const Joi = require('joi');

module.exports.addUser = Joi.object().keys({
    username: Joi.string().regex(/^[a-z A-Z]+$/).required(),
    fullname: Joi.string().regex(/^[a-z A-Z]+$/).required(),
    email: Joi.string().email().required(),
    mobile: Joi.string().regex(/^[0-9]+$/).min(11).max(11).required(),
    roles: Joi.array().items(Joi.string().required())
})

module.exports.updateUser = Joi.object().keys({
    username: Joi.string().regex(/^[a-z A-Z]+$/).required(),
    fullname: Joi.string().regex(/^[a-z A-Z]+$/).required(),
    email: Joi.string().email().required(),
    mobile: Joi.string().regex(/^[0-9]+$/).min(11).max(11).required(),
    isEnabled: Joi.string().required(),
    roles: Joi.array().items(Joi.string().required())
})

module.exports.updatePassword = {
    password: Joi.string().alphanum().min(6).max(10).required()
}

module.exports.resetPassword = {
    email: Joi.string().email().required()
}

