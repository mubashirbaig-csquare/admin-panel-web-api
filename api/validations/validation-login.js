const Joi = require('joi');

module.exports = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().alphanum().min(6).max(10).required()
}).with('email', 'password')


// email: Joi.string().email().required(),
// password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
// access_token: [Joi.string(), Joi.number()],
// birthyear: Joi.number().integer().min(1900).max(2013),
// email: Joi.string().email()


// module.exports  = Joi.object().keys({
//     username: Joi.string().alphanum().min(3).max(30).required(),
//     password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
//     access_token: [Joi.string(), Joi.number()],
//     birthyear: Joi.number().integer().min(1900).max(2013),
//     email: Joi.string().email()
//  }).with('username', 'birthyear').without('password', 'access_token');