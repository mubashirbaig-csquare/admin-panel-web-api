module.exports = function (app, passport) {
      const users = require('./route-users')(app, passport),
            terminal = require('./route-terminals')(app, passport),
            transaction = require('./route-transactions')(app, passport),
            reports = require('./route-reports')(app, passport),
            notifications = require('./route-notifications')(app, passport),
            authentications = require('./route-authentications')(app),            
            middleware = require('./route-error-handler.js')(app)
}

/*
    200 – OK – Eyerything is working
    201 – OK – New resource has been created
    204 – OK – The resource was successfully deleted
    304 – Not Modified – The client can use cached data
    400 – Bad Request – The request was invalid or cannot be served. The exact error should be explained in the error payload. 
          E.g. „The JSON is not valid“
    401 – Unauthorized – The request requires an user authentication
    403 – Forbidden – The server understood the request, but is refusing it or the access is not allowed.
    404 – Not found – There is no resource behind the URI.
    422 – Unprocessable Entity – Should be used if the server cannot process the enitity, 
          e.g. if an image cannot be formatted or mandatory fields are missing in the payload.
    500 – Internal Server Error – API developers should avoid this error. If an error occurs in the global catch blog, 
          the stracktrace should be logged and not returned as response.
*/

/*
    {
  "errors": [
   {
    "userMessage": "Sorry, the requested resource does not exist",
    "internalMessage": "No car found in the database",
    "code": 34,
    "more info": "http://dev.mwaysolutions.com/blog/api/v1/errors/12345"
   }
  ]
} 
*/