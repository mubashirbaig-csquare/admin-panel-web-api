module.exports = function (app, passport) {

    const reportsController = require('../controllers/reports-controller')

    /*
      ****************************
      *    GET ROUTES
      ****************************
    */

    // fetch all terminals
    app.get("/api/v1/terminals-live-uptime", passport.authenticate('jwt', { session: false }), function (req, res) {
        reportsController.getLiveTerminalUpTimeData(req, res)
    })

     // fetch all terminals inventory/cash/cards status
    app.get("/api/v1/terminals-inventory-status", passport.authenticate('jwt', { session: false }), function (req, res) {
        reportsController.getTerminalInventoryStatusData(req, res)
    })

    /*
        ****************************
        *    POST ROUTES
        ****************************
    */

    // fetch all system alerts report
    app.post("/api/v1/reports/alerts-log", passport.authenticate('jwt', { session: false }), function (req, res) {
        reportsController.getAlertsReport(req, res)
    })

    // fetch all audit logs report
    app.post("/api/v1/reports/audit-log", passport.authenticate('jwt', { session: false }), function (req, res) {
        reportsController.getAuditLogReport(req, res)
    })

    // fetch all kiosk details report
    app.post("/api/v1/reports/kiosk-details", passport.authenticate('jwt', { session: false }), function (req, res) {
        reportsController.getKioskDetailReport(req, res)
    })

    // fetch all kiosk summary report
    app.post("/api/v1/reports/kiosk-summary", passport.authenticate('jwt', { session: false }), function (req, res) {
        reportsController.getKioskSummaryReport(req, res)
    })

    // fetch all transaction details report
    app.post("/api/v1/reports/transaction-details", passport.authenticate('jwt', { session: false }), function (req, res) {
        reportsController.getTransactionDetailReport(req, res)
    })

    // fetch all transaction summary report
    app.post("/api/v1/reports/transaction-summary", passport.authenticate('jwt', { session: false }), function (req, res) {
        reportsController.getTransactionSummaryReport(req, res)
    })

    // fetch all terminals uptime report
    app.post("/api/v1/reports/terminals-uptime", passport.authenticate('jwt', { session: false }), function (req, res) {
        reportsController.getTerminalUpTimeReport(req, res)
    })

    // fetch all terminals critical logs report
    app.post("/api/v1/reports/terminals-critical-logs", passport.authenticate('jwt', { session: false }), function (req, res) {
        reportsController.getTerminalCriticalLogsReport(req, res)
    })

}