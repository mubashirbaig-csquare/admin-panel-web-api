module.exports = function (app, passport) {
    const transactionController = require('../controllers/transaction-controller')

/*
 ****************************
 *    GET ROUTES
 ****************************
 */

    // fetch all transaction monitoring data
    app.get("/api/v1/transaction-monitoring", passport.authenticate('jwt', { session: false }), function (req, res) {
        transactionController.getTransactionMonitoringData(req, res)
    })

}