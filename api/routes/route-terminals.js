module.exports = function (app, passport) {
  
  const terminalController = require('../controllers/terminal-controller')

  /*
  ****************************
  *    GET ROUTES
  ****************************
  */

  // fetch all terminals
  app.get("/api/v1/terminals", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.getAllTerminals(req, res)
  })

  // fetch monitoring data for terminals
  app.get("/api/v1/terminals/monitoring", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.getAllTerminalsMonitoringData(req, res) //getTerminalMonitoring
  })

  // fetch all devices related issues for terminals
  app.get("/api/v1/terminals/issue", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.getAllTerminalsIssue(req, res) //getTerminalIssue
  })

  // get terminal details of a specific terminal id
  app.get("/api/v1/terminals/:terminalId", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.getTerminalById(req, res)
  })

  // fetch all regions
  app.get("/api/v1/regions", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.getRegions(req, res)
  })

   // fetch all cities
   app.get("/api/v1/cities", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.getCities(req, res)
  })

  // fetch all branches
  app.get("/api/v1/branches", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.getBranches(req, res)
  })

  // fetch all devices of terminal
  app.get("/api/v1/devices", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.getDevices(req, res)
  })

  // fetch all licenses of terminal
  app.get("/api/v1/licenses", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.getLicenses(req, res)
  })

 
  /*
  ****************************
  *    POST ROUTES
  ****************************
  */

  // for adding a new terminal with it's devices
  app.post("/api/v1/terminals", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.addNewTerminalWithDevices(req, res)
  })

   // it is a route for generating license
   app.post("/api/v1/generate-licence/:numberOfLicenses", function(req, res){
    terminalController.generateLicense(req, res)
  })


  /*
  ****************************
  *    PUT ROUTES
  ****************************
  */


  // for updating sepcific terminal's critical status
  app.put("/api/v1/terminals/:terminalId/critical-status", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.updateTerminalCriticalStatus(req, res)
  })

  // for updating serial numbers of newly added terminal
  app.put("/api/v1/terminals/:terminalId/devices", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.updateSerialNumbersOfAllDevicesOfATerminal(req, res) //updateTerminalDevicesSerialNumber
  })

  // for updating specific terminal's device serial number 
  app.put("/api/v1/terminals/:terminalDeviceId/:serialNumber", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.updateSerialNumberOfSpecificDeviceOfATerminal(req, res) //updateTerminalDeviceSerialNumber
  })  

  // for updating details of a specific terminal
  app.put("/api/v1/terminals/:terminalId", passport.authenticate('jwt', { session: false }), function(req, res){
    terminalController.updateTerminal(req, res)
  })

  }