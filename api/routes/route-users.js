module.exports = function (app, passport) {
  const userController = require('../controllers/user-controller')
  const authController = require('../controllers/auth-controller')

   /*
  ****************************
  *    GET ROUTES
  ****************************
  */

  app.get("/api/v1/users", passport.authenticate('jwt', { session: false }), function(req, res){
    userController.getUsers(req, res)
  })

  app.get("/api/v1/users/:userId", passport.authenticate('jwt', { session: false }), function(req, res){
    userController.getUserById(req, res)
  })

  app.get("/api/v1/roles", passport.authenticate('jwt', { session: false }), function(req, res){
    userController.getRoles(req, res)
  })

// =================================================================================================
 
  /*
  ****************************
  *    POST ROUTES
  ****************************
  */

  app.post("/api/v1/users", passport.authenticate('jwt', { session: false }), function(req, res){
    userController.addUser(req, res)
  })

// =================================================================================================
 
  /*
  ****************************
  *    PUT ROUTES
  ****************************
  */

  app.put("/api/v1/users/:userId/resetPassword", passport.authenticate('jwt', { session: false }), function(req, res){
    userController.resetPassword(req, res)
  })

  app.put("/api/v1/users/:userId", passport.authenticate('jwt', { session: false }), function(req, res){
    userController.updateUser(req, res)
  })

  app.put("/api/v1/users/:userId/password", passport.authenticate('jwt', { session: false }), function(req, res){
    userController.updatePassword(req, res)
  })
  


}