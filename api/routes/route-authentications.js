module.exports = function (app) {
    const authController = require('../controllers/auth-controller')
    app.route('/api/v1/authentications')
    .post(authController.verifyUserCredentials)
}