
const appConfig = require('../../app-config'),
    utils = require('../utils'),
    fileNameForLogging = 'route-error-handler'

import Error from '../response-classes/error'

module.exports = function (app) {
    app.use(function (req, res, next) {
        utils.doErrorLog(fileNameForLogging, req.url, 'invalid route accessed.')
        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, "invalid route")
        res.status(404).json({ response: errorResponse })
    });
}

