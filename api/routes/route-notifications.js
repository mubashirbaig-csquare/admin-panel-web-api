module.exports = function (app, passport) {

    const notificationController = require('../controllers/notification-controller')

    /*
      ****************************
      *    GET ROUTES
      ****************************
    */

    // fetch all notifcation types 
    app.get("/api/v1/notifications-list", passport.authenticate('jwt', { session: false }), function (req, res) {
        notificationController.getNotificationType(req, res)
    })

    //fetch all emails associated with notifications
    app.get("/api/v1/emails-notifications-list", passport.authenticate('jwt', { session: false }), function (req, res) {
        notificationController.getEmailsNotificationsList(req, res)
    })

    //fetch all emails associated with specific notification name
    app.get("/api/v1/emails-notification/:notificationName", passport.authenticate('jwt', { session: false }), function (req, res) {
        notificationController.getEmailsNotificationByNotificationName(req, res)
    })

    /*
        ****************************
        *    PUT ROUTES
        ****************************
    */
    app.put("/api/v1/emails-notification/", passport.authenticate('jwt', { session: false }), function(req, res){
        notificationController.updateEmailsNotification(req, res)
      })

}