export default class Error {
    constructor(userMessage, internalMessage, stackTrace) {
        this.userMessage = userMessage
        this.internalMessage = internalMessage
        this.stackTrace = stackTrace        
    }

    // getErrorObject(error) {
    //     return this.error
    // }
}

// {
//     "errors": [
//      {
//       "userMessage": "Sorry, the requested resource does not exist",
//       "internalMessage": "No car found in the database",
//       "code": 34,
//       "more info": "http://dev.mwaysolutions.com/blog/api/v1/errors/12345"
//      }
//     ]
//   } 