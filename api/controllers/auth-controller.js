const Joi = require('joi'),
    authValidation = require('../validations/validation-auth'),
    md5 = require('md5'),
    Sequelize = require('sequelize'),
    jwt = require('jsonwebtoken'),
    appConfig = require('../../app-config'),
    log = require('../utils'),
    db = require('../../db').getInstance(),
    fileNameForLogging = 'auth-controller'

import Error from '../response-classes/error'


// ==============================================================================================


exports.verifyUserCredentials = function (req, res) {
    log.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    const { email, password } = req.body
    Joi.validate({ email, password }, authValidation.verifyUserCredentials, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'invalid params : ', err)
            const errorResponse = new Error(err.details[0].message, err.message, err);
            res.status(401).json({ response: errorResponse })
        } else {
            const encryptedPassword = md5(password)
            db.query(`  SELECT * FROM Users 
            INNER JOIN UserinRoles  ON UserinRoles.UserID=Users.UserID
            INNER JOIN Roles  ON UserinRoles.RoleID=Roles.RoleID
            where Users.Email='${email}';`).spread((results, metadata) => {
                    if (results !== null) {
                        const user = results[0]
                        if (user.Password !== encryptedPassword) {
                            log.doErrorLog(fileNameForLogging, req.url, 'wrong password.')
                            const errorResponse = new Error('Authentication failed! Wrong password.', 'wrong password.', '')
                            res.status(401).json({ response: errorResponse })
                        } else if (!user.IsEnabled) {
                            log.doErrorLog(fileNameForLogging, req.url, 'user disabled.')
                            const errorResponse = new Error('Sorry you do not have access. Please contact your administrator.', 'user disabled', '')
                            res.status(401).json({ response: errorResponse })
                        } else {
                            var token = jwt
                                        .sign(
                                                { userId: user.UserID, userRole: user.RoleName },
                                                appConfig.JWT_SECRET_KEY,
                                                { expiresIn: appConfig.JWT_EXPIRY_TIME }
                                            )
                            if (token != null) {
                                res.status(200).json({ 
                                    username: user.Username,
                                    fullName: user.FullName,
                                    userId: user.UserID,
                                    lastLoginDate: user.LastLoginDate,
                                    roleName: user.RoleName,
                                    roleId: user.RoleID,
                                    token
                                })
                            } else {
                                log.doFatalLog(fileNameForLogging, req.url, 'token generation failed.')
                                const errorResponse = new Error('Sorry unable to proccess. Please try again!', 'token generation failed!')
                                res.status(500).json({ response: errorResponse })
                            }
                        }
                    } else {
                        log.doFatalLog(fileNameForLogging, req.url, 'auth failed.')
                        const errorResponse = new Error('Authentication failed! User not found.', 'user do not exist.', '')
                        res.status(401).json({ response: errorResponse })
                    }
                })
                .catch(err => {
                    log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                    const errorResponse = new Error('Sorry unable to proccess. Please try again!', 'database error')
                    res.status(500).json({ response: errorResponse })
                });
        }
    })
}


// ==============================================================================================


// exports.auth = function (req, res, next) {
//     const token = req.headers['authorization'].split(" ")[1]
//     if (token != null) {
//         jwt.verify(token, appConfig.JWT_SECRET_KEY, function (err, decoded) {
//             if (err) {
//                 log.doFatalLog(fileNameForLogging, req.url, 'exception in token verification : ', err)
//                 const errorResponse = new Error('Access not allowed.', err.message, err)
//                 res.status(401).json({ Error: errorResponse })
//             } else {
//                 next()
//             }
//         })
//     } else {
//         log.doErrorLog(fileNameForLogging, req.url, 'token field empty.')
//         const errorResponse = new Error('Access not allowed.', 'token field empty.', '')
//         return res.status(401).json({ Error: errorResponse })
//     }
// }



// ==============================================================================================