const appConfig = require('../../app-config'),
    Joi = require('joi'),
    //reportsValidation = require('../validations/validation-reports'),
    db = require('../../db').getInstance(),
    log = require('../utils'),
    fileNameForLogging = 'notifcation-controller'

import Error from '../response-classes/error'

exports.getNotificationType = function (req, res) {
    db.query(`  SELECT [NotificationID],[NotificationAlert]
                FROM [NotificationType] 
                order by [NotificationAlert] asc`)
        .then(data => {
            log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            const queryResponse = data[0]
            if (queryResponse.length > 0) {
                res.status(200).json({ response: queryResponse })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'no data returned from db')
                const errorResponse = new Error(appConfig.NO_DATA_ERROR_MSG)
                res.status(204).json({ response: errorResponse })
            }
        })
        .catch(err => {
            log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

exports.getEmailsNotificationsList = function (req, res) {
    db.query(`  SELECT nt.NotificationAlert,[EmailTo],[EmailCC]
                FROM [NotificationType] nt left join [NotificationEmail] ne
                On nt.NotificationID =ne.NotificationType
                Order by nt.NotificationAlert ASC`)
        .then(data => {
            log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            const queryResponse = data[0]
            if (queryResponse.length > 0) {
                res.status(200).json({ response: queryResponse })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'no data returned from db')
                const errorResponse = new Error(appConfig.NO_DATA_ERROR_MSG)
                res.status(204).json({ response: errorResponse })
            }
        })
        .catch(err => {
            log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

exports.getEmailsNotificationByNotificationName = (req, res) => {
    db.query(`  SELECT nt.NotificationAlert,[EmailTo],[EmailCC]
                FROM [NotificationType] nt left join [NotificationEmail] ne
                On nt.NotificationID =ne.NotificationType 
                Where nt.NotificationAlert='${ req.params.notificationName}'`)
        .then(data => {
            log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            res.status(200).json({ response: data[0] })
        })
        .catch(err => {
            log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

exports.updateEmailsNotification = (req, res) => {
    let NotificationType = req.body.notificationAlert;
    let EmailTo = req.body.emailTo;
    let EmailCC = req.body.emailCC;
   
    db.query(`  if (SELECT count(nt.NotificationAlert)
                    FROM [NotificationType] nt,[NotificationEmail] ne
                    Where nt.NotificationID =ne.NotificationType 
                    And nt.NotificationAlert = 'Casette Empty') 
                > 0 
                begin
                    Update NotificationEmail
                    Set EmailTo= '${EmailTo}',
                    EmailCC='${EmailCC}'
                    Where NotificationType =
                        (Select NotificationID
                        From NotificationType
                        Where NotificationAlert='${NotificationType}')
                end 
                Else begin
                Insert into NotificationEmail 
                (EmailTo,EmailCC,NotificationType)
                Values
                ('${EmailTo}','${EmailCC}',
                    (Select NotificationID 
                    From NotificationType 
                    Where NotificationAlert='${NotificationType}'))
                End`)
        .then(data => {
            res.status(200).json({ response: "success" })
        })
        .catch(err => {
            log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

