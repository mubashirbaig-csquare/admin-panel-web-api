const appConfig = require('../../app-config'),
    Joi = require('joi'),
    Sequelize = require('sequelize'),
    utils = require('../utils'),
    db = require('../../db').getInstance(),
    _ = require('lodash'),
    fileNameForLogging = 'transaction-controller'

import Error from '../response-classes/error'


// ==============================================================================================

/**
 * ===============================================
 *  GET REQUESTS METHODS
 * ===============================================
 **/

exports.getTransactionMonitoringData = (req, res) => {
    db.query(`Admin_GetTerminalTransactionLog`)
        .then(users => {
            utils.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            return res.status(200).json({
                response: {
                    AccOpenCount: users[0][0].AccOpenCount == null ? 0 : users[0][0].AccOpenCount,
                    CardDespCount: users[0][0].CardDespCount == null ? 0 : users[0][0].CardDespCount,
                    TotCashDeposit: users[0][0].TotCashDeposit == null ? 0 : users[0][0].TotCashDeposit
                }
            })
        })
        .catch(err => {
            utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}


// ==============================================================================================