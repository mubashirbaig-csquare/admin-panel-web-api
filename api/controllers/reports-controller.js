const appConfig = require('../../app-config'),
    Joi = require('joi'),
    reportsValidation = require('../validations/validation-reports'),
    db = require('../../db').getInstance(),
    log = require('../utils'),
    fileNameForLogging = 'reports-controller'

import Error from '../response-classes/error'

// ==============================================================================================

/**
 * ===============================================
 *  GET REQUESTS METHODS
 * ===============================================
 **/

exports.getAlertsReport = function (req, res) {
    const { fromDate, toDate } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    Joi.validate({ fromDate, toDate }, reportsValidation.fromDateAndToDateParamsCheck, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'invalid params')
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, 'invalid params')
            res.status(400).json({ response: errorResponse })
        } else {
            db.query(`  SELECT DeviceName, Description, format(SystemAlert.CreatedDate,'dd-MMM-yyyy hh:mm:ss tt') as UpdateDate , TerminalName FROM SystemAlert
                        JOIN Terminal on Terminal.TerminalID = SystemAlert.TerminalID 
                        JOIN Device on Device.DeviceID = SystemAlert.DeviceID  
                        WHERE SystemAlert.CreatedDate BETWEEN '${fromDate}' AND '${toDate}' `)
                .then(data => {
                    log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')

                    const queryResponse = data[0]
                    if (queryResponse.length > 0) {
                        let responseData = queryResponse.map((value, index, queryResponse) => {
                            return {
                                TerminalName: value.TerminalName,
                                DeviceName: value.DeviceName,
                                Description: value.Description,
                                LogDate: value.UpdateDate
                            }
                        })
                        res.status(200).json({ response: responseData })
                    } else {
                        log.doErrorLog(fileNameForLogging, req.url, 'no data returned from db')
                        const errorResponse = new Error(appConfig.NO_DATA_ERROR_MSG)
                        res.status(204).json({ response: errorResponse })
                    }
                })
                .catch(err => {
                    log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    res.status(500).json({ response: errorResponse })
                });
        }
    })
}

exports.getAuditLogReport = function (req, res) {
    const { fromDate, toDate } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    Joi.validate({ fromDate, toDate }, reportsValidation.fromDateAndToDateParamsCheck, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'invalid params')
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, 'invalid params')
            res.status(400).json({ response: errorResponse })
        } else {
            db.query(`  SELECT format(LogDateTime,'dd-MMM-yyyy hh:mm:ss tt') as UpdateDate,Action,ActionValue,CNIC,TerminalName 
                        FROM ActivityLog 
                        JOIN Terminal on Terminal.TerminalID = ActivityLog.TerminalID
                        WHERE LogDateTime BETWEEN '${fromDate}' AND '${toDate}' `)
                .then(data => {
                    log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')

                    const queryResponse = data[0]
                    if (queryResponse.length > 0) {
                        let responseData = queryResponse.map((value, index, queryResponse) => {
                            return {
                                TerminalName: value.TerminalName,
                                Action: value.Action,
                                ActionStatus: value.ActionValue,
                                CNIC: value.CNIC,
                                LogDate: value.UpdateDate
                            }
                        })
                        res.status(200).json({ response: responseData })
                    } else {
                        log.doErrorLog(fileNameForLogging, req.url, 'no data returned from db')
                        const errorResponse = new Error(appConfig.NO_DATA_ERROR_MSG)
                        res.status(204).json({ response: errorResponse })
                    }
                })
                .catch(err => {
                    log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    res.status(500).json({ response: errorResponse })
                });
        }
    })
}

exports.getKioskDetailReport = function (req, res) {
    const { fromDate, toDate } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    Joi.validate({ fromDate, toDate }, reportsValidation.fromDateAndToDateParamsCheck, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'invalid params')
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, 'invalid params')
            res.status(400).json({ response: errorResponse })
        } else {
            db.query(`  SELECT TerminalName, format(LogDate,'dd-MMM-yyyy hh:mm:ss tt') as UpdateDate, TransactionName, CNIC, CashIn
                        FROM TerminalTransactionLog
                        JOIN TransactionName on TransactionName.TransactionTypeID = TerminalTransactionLog.TransactionTypeID
                        JOIN Terminal On Terminal.TerminalID = TerminalTransactionLog.TerminalID
                        WHERE LogDate BETWEEN '${fromDate}' AND '${toDate}' `)
                .then(data => {
                    log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')

                    const queryResponse = data[0]
                    if (queryResponse.length > 0) {
                        let responseData = queryResponse.map((value, index, queryResponse) => {
                            return {
                                TerminalName: value.TerminalName,
                                CNIC: value.CNIC,
                                TransactionName: value.TransactionName,
                                CashIn: value.CashIn,
                                LogDate: value.UpdateDate
                            }
                        })
                        res.status(200).json({ response: responseData })
                    } else {
                        log.doErrorLog(fileNameForLogging, req.url, 'no data returned from db')
                        const errorResponse = new Error(appConfig.NO_DATA_ERROR_MSG)
                        res.status(204).json({ response: errorResponse })
                    }
                })
                .catch(err => {
                    log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    res.status(500).json({ response: errorResponse })
                });
        }
    })
}

exports.getKioskSummaryReport = function (req, res) {
    const { fromDate, toDate } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    Joi.validate({ fromDate, toDate }, reportsValidation.fromDateAndToDateParamsCheck, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'invalid params')
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, 'invalid params')
            res.status(400).json({ response: errorResponse })
        } else {
            db.query(`  SELECT format( LogDate,'dd-MMM-yyyy hh:mm:ss tt') as UpdateDate,
                        COUNT(case when TerminalTransactionLog.TransactionTypeID = 1 Then 1 else null End)  AccOpenCount,
                        COUNT(case when TerminalTransactionLog.TransactionTypeID = 2 Then 1 else null End)  CardDespCount,
                        SUM(case when TerminalTransactionLog.TransactionTypeID = 3 Then CashIn else 0 End)  TotCashDeposit
                        FROM TerminalTransactionLog
                        JOIN TransactionName On TransactionName.TransactionTypeID = TerminalTransactionLog.TransactionTypeID
                        WHERE DateAdd(day, DateDiff(day, 0, LogDate), 0) BETWEEN '${fromDate}' AND '${toDate}'
                        GROUP BY format( LogDate,'dd-MMM-yyyy hh:mm:ss tt')  `)
                .then(data => {
                    log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')

                    const queryResponse = data[0]
                    if (queryResponse.length > 0) {
                        let responseData = queryResponse.map((value, index, queryResponse) => {
                            return {
                                AccountOpeningCount: value.AccOpenCount == null ? 0 : value.AccOpenCount,
                                DispenseCount: value.CardDespCount == null ? 0 : value.CardDespCount,
                                CashDepositCount: value.TotCashDeposit == null ? 0 : value.TotCashDeposit,
                                LogDate: value.UpdateDate
                            }
                        })
                        res.status(200).json({ response: responseData })
                    } else {
                        log.doErrorLog(fileNameForLogging, req.url, 'no data returned from db')
                        const errorResponse = new Error(appConfig.NO_DATA_ERROR_MSG)
                        res.status(204).json({ response: errorResponse })
                    }
                })
                .catch(err => {
                    log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    res.status(500).json({ response: errorResponse })
                });
        }
    })
}

exports.getTransactionDetailReport = function (req, res) {
    const { fromDate, toDate } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    Joi.validate({ fromDate, toDate }, reportsValidation.fromDateAndToDateParamsCheck, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'invalid params')
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, 'invalid params')
            res.status(400).json({ response: errorResponse })
        } else {
            db.query(`  SELECT Terminal.TerminalName,TransactionName.TransactionName, format(TransactionLogs.CreatedDate,'dd-MMM-yyyy hh:mm:ss tt') as UpdateDate, ResponseCode, ResponseDescription
                        FROM TransactionLogs
                        JOIN Terminal On Terminal.TerminalID = TransactionLogs.TerminalID
                        JOIN TransactionName On TransactionName.TransactionTypeID = TransactionLogs.TransactionNameID
                        WHERE TransactionLogs.TerminalID = 1 and TransactionLogs.TransactionTypeID = 2 AND
                        DateAdd(day, DateDiff(day, 0, TransactionLogs.CreatedDate), 0) BETWEEN '${fromDate}' AND '${toDate}' `)
                .then(data => {
                    log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')

                    const queryResponse = data[0]
                    if (queryResponse.length > 0) {
                        let responseData = queryResponse.map((value, index, queryResponse) => {
                            return {
                                TerminalName: value.TerminalName,
                                TransactionName: value.TransactionName,
                                Response: value.ResponseCode,
                                ResponseDescription: value.ResponseDescription,
                                LogDate: value.UpdateDate
                            }
                        })
                        res.status(200).json({ response: responseData })
                    } else {
                        log.doErrorLog(fileNameForLogging, req.url, 'no data returned from db')
                        const errorResponse = new Error(appConfig.NO_DATA_ERROR_MSG)
                        res.status(204).json({ response: errorResponse })
                    }
                })
                .catch(err => {
                    log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    res.status(500).json({ response: errorResponse })
                });
        }
    })
}

exports.getTransactionSummaryReport = function (req, res) {
    const { fromDate, toDate } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    Joi.validate({ fromDate, toDate }, reportsValidation.fromDateAndToDateParamsCheck, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'invalid params')
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, 'invalid params')
            res.status(400).json({ response: errorResponse })
        } else {
            db.query(`  SELECT format( TransactionLogs.CreatedDate,'dd-MMM-yyyy hh:mm:ss tt') as UpdateDate,
                        Terminal.TerminalName,TransactionName.TransactionName, ResponseCode, count(*) TotalCount
                        FROM TransactionLogs
                        Join Terminal On Terminal.TerminalID = TransactionLogs.TerminalID
                        Join TransactionName On TransactionName.TransactionTypeID = TransactionLogs.TransactionNameID
                        where TransactionLogs.TerminalID = 1 and TransactionLogs.TransactionTypeID = 2 and
                        DateAdd(day, DateDiff(day, 0, TransactionLogs.CreatedDate), 0) BETWEEN '${fromDate}' AND '${toDate}' 
                        GROUP BY format( TransactionLogs.CreatedDate,'dd-MMM-yyyy hh:mm:ss tt'),
                        Terminal.TerminalName,TransactionName.TransactionName, ResponseCode `)
                .then(data => {
                    log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')

                    const queryResponse = data[0]
                    if (queryResponse.length > 0) {
                        let responseData = queryResponse.map((value, index, queryResponse) => {
                            return {
                                TerminalName: value.TerminalName,
                                TransactionName: value.TransactionName,
                                Response: value.ResponseCode,
                                TotalCount: value.TotalCount,
                                LogDate: value.UpdateDate
                            }
                        })
                        res.status(200).json({ response: responseData })
                    } else {
                        log.doErrorLog(fileNameForLogging, req.url, 'no data returned from db')
                        const errorResponse = new Error(appConfig.NO_DATA_ERROR_MSG)
                        res.status(204).json({ response: errorResponse })
                    }
                })
                .catch(err => {
                    log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    res.status(500).json({ response: errorResponse })
                });
        }
    })
}

exports.getTerminalUpTimeReport = function (req, res) {
    const { fromDate, toDate } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    Joi.validate({ fromDate, toDate }, reportsValidation.fromDateAndToDateParamsCheck, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'invalid params')
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, 'invalid params')
            res.status(400).json({ response: errorResponse })
        } else {
            db.query(`  SELECT TerminalName,format(EventDateTime,'dd-MMM-yyyy hh:mm:ss tt') as UpdateDate, NetworkStatus, Reason 
                        FROM TerminalNetworkStatus 
                        JOIN Terminal ON TerminalNetworkStatus.TerminalID = Terminal.TerminalID
                        WHERE EventDateTime BETWEEN '${fromDate}' AND '${toDate}' `)
                .then(data => {
                    log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')

                    const queryResponse = data[0]
                    if (queryResponse.length > 0) {
                        let responseData = queryResponse.map((value, index, queryResponse) => {
                            return {
                                TerminalName: value.TerminalName,
                                NetworkStatus: value.NetworkStatus == 1 ? 'Connected' : 'Disconnected',
                                Reason: value.Reason,
                                LogDate: value.UpdateDate
                            }
                        })
                        res.status(200).json({ response: responseData })
                    } else {
                        log.doErrorLog(fileNameForLogging, req.url, 'no data returned from db')
                        const errorResponse = new Error(appConfig.NO_DATA_ERROR_MSG)
                        res.status(204).json({ response: errorResponse })
                    }
                })
                .catch(err => {
                    log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    res.status(500).json({ response: errorResponse })
                });
        }
    })
}

exports.getTerminalCriticalLogsReport = function (req, res) {
    const { fromDate, toDate } = req.body
    log.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    Joi.validate({ fromDate, toDate }, reportsValidation.fromDateAndToDateParamsCheck, function (err, value) {
        if (err) {
            log.doErrorLog(fileNameForLogging, req.url, 'invalid params')
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, 'invalid params')
            res.status(400).json({ response: errorResponse })
        } else {
            db.query(`  SELECT TerminalName, UserName, Comments, format(TerminalCriticalLog.CreatedDate,'dd-MMM-yyyy hh:mm:ss tt') as UpdateDate
                        FROM TerminalCriticalLog
                        JOIN Terminal ON TerminalCriticalLog.TerminalID = Terminal.TerminalID
                        JOIN Users ON Users.UserID = TerminalCriticalLog.UserID
                        WHERE TerminalCriticalLog.CreatedDate BETWEEN '${fromDate}' AND '${toDate}' `)
                .then(data => {
                    log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')

                    const queryResponse = data[0]
                    if (queryResponse.length > 0) {
                        let responseData = queryResponse.map((value, index, queryResponse) => {
                            return {
                                TerminalName: value.TerminalName,
                                UserName: value.UserName,
                                Comments: value.Comments,
                                LogDate: value.UpdateDate
                            }
                        })
                        res.status(200).json({ response: responseData })
                    } else {
                        log.doErrorLog(fileNameForLogging, req.url, 'no data returned from db')
                        const errorResponse = new Error(appConfig.NO_DATA_ERROR_MSG)
                        res.status(204).json({ response: errorResponse })
                    }
                })
                .catch(err => {
                    log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    res.status(500).json({ response: errorResponse })
                });
        }
    })
}

exports.getLiveTerminalUpTimeData = function (req, res) {
    db.query(`  SELECT TOP 20 TerminalName, format(EventDateTime,'dd-MMM-yyyy hh:mm:ss tt') as UpdateDate, NetworkStatus, Reason 
                FROM TerminalNetworkStatus 
                JOIN Terminal ON TerminalNetworkStatus.TerminalID = Terminal.TerminalID 
                ORDER BY EventDateTime desc`)
        .then(data => {
            log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            const queryResponse = data[0].reverse()
            if (queryResponse.length > 0) {
                let responseData = queryResponse.map((value, index, queryResponse) => {
                    return {
                        TerminalName: value.TerminalName,
                        NetworkStatus: value.NetworkStatus == 1 ? 'Connected' : 'Disconnected',
                        Reason: value.Reason,
                        LogDate: value.UpdateDate
                    }
                })
                res.status(200).json({ response: responseData })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'no data returned from db')
                const errorResponse = new Error(appConfig.NO_DATA_ERROR_MSG)
                res.status(204).json({ response: errorResponse })
            }
        })
        .catch(err => {
            log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

exports.getTerminalInventoryStatusData = function (req, res) {
    db.query(`  select Terminal.TerminalName, isnull(TotalCash,0) TotalCash, isnull(Note10,0) Note10, isnull(Note20,0) Note20, isnull(Note50,0) Note50, isnull(Note100,0) Note100, isnull(Note500,0) Note500, isnull(Note1000,0) Note1000, isnull(Note5000,0) Note5000,
                isnull(TotalCards,0) TotalCards, isnull(TotalRejected ,0) TotalRejected, isnull(TotalDispensed ,0) TotalDispensed,  isnull(TotalRetracted ,0) TotalRetracted,
                isnull(TotalCards,0) - (isnull(TotalRejected ,0) + isnull(TotalDispensed ,0) + isnull(TotalRetracted ,0)) AvailableCard
                From Terminal
                left Join (
                    Select activitylog.terminalID, sum(case when ISNUMERIC( ActionValue) = 1 then cast (ActionValue as int) else 0 end) TotalCash,
                    sum(case when Action = 'Cash Deposit'  and  ActionValue = '10' Then 1 else 0 end) Note10,
                    sum(case when Action = 'Cash Deposit'  and ActionValue = '20' Then 1 else 0 end) Note20,
                    sum(case when Action = 'Cash Deposit'  and ActionValue = '50' Then 1 else 0 end) Note50,
                    sum(case when Action = 'Cash Deposit'  and ActionValue = '100' Then 1 else 0 end) Note100,
                    sum(case when Action = 'Cash Deposit'  and ActionValue = '500' Then 1 else 0 end) Note500,
                    sum(case when Action = 'Cash Deposit'  and ActionValue = '1000' Then 1 else 0 end) Note1000,
                    sum(case when Action = 'Cash Deposit'  and ActionValue = '5000' Then 1 else 0 end) Note5000
                    from ActivityLog
                    where ActivityLog.TerminalID = 1 and  ActivityLogID > (select max(ActivityLogID) from ActivityLog where Action = 'NoteAcceptorStatus' and ActionValue = 'CassetteRemove')
                    group by activitylog.terminalID
                ) As CashCount On Terminal.TerminalID = CashCount.terminalID
                left join (
                    Select activitylog.terminalID, 
                    sum(case when Action = 'Total Debit Cards in Hopper' Then case when ISNUMERIC( ActionValue) = 1 then cast (ActionValue as int) else 0 end end) TotalCards,
                    sum(case when Action = 'Debit Card Rejected' Then 1 else 0 end) TotalRejected,
                    sum(case when Action = 'Debit Card Dispensed' Then 1 else 0 end) TotalDispensed,
                    sum(case when Action = 'Debit Card Retracted' Then 1 else 0 end) TotalRetracted
                    from ActivityLog
                    where ActivityLog.TerminalID = 1 and  ActivityLogID >= (select max(ActivityLogID) from ActivityLog where Action = 'Total Debit Cards in Hopper')
                    group by activitylog.terminalID
                ) as TotalCards on Terminal.TerminalID = TotalCards.terminalID`)
        .then(data => {
            log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            if (data[0].length > 0) {
                res.status(200).json({ response: data[0] })
            } else {
                log.doErrorLog(fileNameForLogging, req.url, 'no data returned from db')
                const errorResponse = new Error(appConfig.NO_DATA_ERROR_MSG)
                res.status(204).json({ response: errorResponse })
            }
        })
        .catch(err => {
            log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}
