const appConfig = require('../../app-config'),
    Joi = require('joi'),
    Sequelize = require('sequelize'),
    terminalDevicesModel = require('../models/models-terminal-devices'),
    terminalValidation = require('../validations/validation-terminals'),
    utils = require('../utils'),
    db = require('../../db').getInstance(),
    _ = require('lodash'),
    fileNameForLogging = 'terminal-controller'

import Error from '../response-classes/error'


// ==============================================================================================

/**
 * ===============================================
 *  GET REQUESTS METHODS
 * ===============================================
 **/

exports.getAllTerminalsMonitoringData = (req, res) => {
    db.query(`  SELECT a.TerminalID,a.TerminalName,a.Status,format(a.UpdateDate,'dd-MMM-yyyy hh:mm:ss tt') as UpdateDate,c.DeviceName,b.DeviceStatus,b.Reason,d.BranchName,
                CASE WHEN a.Status = 0 or a.isCritical = 1 or a.isAdminLock = 1 then 0 else 1 End OverallTerminalStatus,
                CASE WHEN a.Status = 0 then 'Network Disconnect' else 
                CASE when a.isCritical = 1 then 'Terminal Lock because of device(s) fault' else 
                CASE when a.isAdminLock = 1 then 'Lock by Administrator' else '' End  end end OverallTerminalReason
                FROM Terminal as a
                INNER JOIN TerminalDevices as b ON a.TerminalID=b.TerminalID
                INNER JOIN Device as c ON c.DeviceID=b.DeviceID
                INNER JOIN Branch as d ON d.BranchID = a.BranchID`)
        .then(users => {
            utils.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            let result = [];
            let arrOfObjects = []
            let count = 0
            const map = new Map();
            let responseArray = []
            const tempMapForCreatingTerminalWithDevicesObject = new Map()
            let pasta;
            for (let user of users[0]) {
                if (tempMapForCreatingTerminalWithDevicesObject.has(user.TerminalID)) {
                    pasta.devices.push({
                        deviceName: user.DeviceName,
                        deviceStatus: user.DeviceStatus,
                        deviceReason: user.Reason
                    })
                    result.push(user)
                } else {
                    if (count > 0) {
                        responseArray.push(pasta)
                    }
                    pasta = null
                    pasta = new Object()
                    pasta.terminalID = user.TerminalID
                    pasta.terminalName = user.TerminalName
                    pasta.status = user.Status
                    pasta.branchName = user.BranchName
                    pasta.overallTerminalStatusReason = user.OverallTerminalReason
                    pasta.updateDate = user.UpdateDate//moment(user.UpdateDate).format("dddd, MMMM Do YYYY, h:mm:ss a")//user.UpdateDate
                    if (pasta.updateDate == 'Invalid date') {
                        pasta.updateDate = 'N/A'
                    }
                    pasta.overallTerminalStatus = user.OverallTerminalStatus
                    pasta.devices = [
                        {
                            deviceName: user.DeviceName,
                            deviceStatus: user.DeviceStatus,
                            deviceReason: user.Reason
                        }
                    ]
                    result = []
                    count = 0
                    result.push(user)
                    count++
                    tempMapForCreatingTerminalWithDevicesObject.set(user.TerminalID, pasta);
                }
            }
            responseArray.push(pasta)
            return res.status(200).json({ response: responseArray })
        })
        .catch(err => {
            utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

exports.getAllTerminalsIssue = (req, res) => {
    db.query('Admin_GetCriticalErrorDetail')
        .then(data => {
            let responseData = []
            for (let value of data[0]) {
                responseData.push(value)
            }
            utils.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            res.status(200).json({ response: responseData })
        })
        .catch(err => {
            utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

exports.getAllTerminals = (req, res) => {
    db.query(`  SELECT a.TerminalID,a.TerminalName,a.IpAddress,a.MacAddress,a.KeepAliveInterval,b.BranchName,a.Status, a.isAdminLock
                FROM Terminal as a 
                INNER JOIN Branch as b 
                ON b.BranchID=a.BranchID`)
        .then(data => {
            utils.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            res.status(200).json({ response: data[0] })
        })
        .catch(err => {
            utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

exports.getTerminalById = (req, res) => {
    db.query(`SELECT a.TerminalID,a.isAdminLock,a.TerminalName,a.IpAddress,a.MacAddress,a.KeepAliveInterval,b.BranchName,a.Status, d.DeviceName,d.DeviceID,c.SerialNo,c.TerminalDeviceID, f.RegionID, e.CityID, b.BranchID, a.LicenseID, g.LicenseKey
                FROM Terminal as a 
                INNER JOIN Branch as b 
                ON b.BranchID=a.BranchID
                INNER JOIN TerminalDevices as c 
                ON c.TerminalID=a.TerminalID
                INNER JOIN Device as d 
                ON c.DeviceID=d.DeviceID 			
                INNER JOIN City as e
                ON e.CityID=b.CityID 
                INNER JOIN Region as f 
                ON f.RegionID=e.RegionID 
                INNER JOIN License as g 
                ON g.LicenseID=a.LicenseID 
                WHERE a.TerminalID = ${ req.params.terminalId}`)
        .then(data => {
            utils.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            res.status(200).json({ response: data[0] })
        })
        .catch(err => {
            utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

exports.getRegions = (req, res) => {
    db.query(`SELECT * FROM Region`)
        .then(data => {
            utils.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            res.status(200).json({ response: data[0] })
        })
        .catch(err => {
            utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

exports.getDevices = (req, res) => {
    db.query(`SELECT * FROM Device`)
        .then(data => {
            utils.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            res.status(200).json({ response: data[0] })
        })
        .catch(err => {
            utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

exports.getCities = (req, res) => {
    db.query(`SELECT * FROM City`)
        .then(data => {
            utils.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            res.status(200).json({ response: data[0] })
        })
        .catch(err => {
            utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

exports.getLicenses = (req, res) => {
    db.query(`SELECT * FROM License`)
        .then(data => {
            utils.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            res.status(200).json({ response: data[0] })
        })
        .catch(err => {
            utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

exports.getBranches = (req, res) => {
    db.query(`SELECT * FROM Branch`)
        .then(data => {
            utils.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
            res.status(200).json({ response: data[0] })
        })
        .catch(err => {
            utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
            const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
            res.status(500).json({ response: errorResponse })
        });
}

// ==============================================================================================
// ==============================================================================================

/**
 * ===============================================
 *  POST REQUESTS METHODS
 * ===============================================
 **/

exports.addNewTerminalWithDevices = (req, res) => {
    utils.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    const { terminalName, ipAddress, macAddress, keepAliveInterval, branchId, licenseId, licenseKey, userId } = req.body
    const terminalDeviceId = req.params.terminalDeviceId

    Joi.validate({ terminalName, ipAddress, macAddress, keepAliveInterval, branchId, licenseId, licenseKey, userId }, terminalValidation.addNewTerminalWithDevices, function (err, value) {
        if (err) {
            utils.doErrorLog(fileNameForLogging, req.url, 'invalid params : ', err)
            res.status(400).json(new Error(err.details[0].message, err.message, err))
        } else {
            const decryptedLicenseKey = utils.decrypt(licenseKey)
            if (decryptedLicenseKey != '') {
                let licenseCount = decryptedLicenseKey.substring(0, 4)
                db.query(`SELECT COUNT(LicenseID) as TerminalLicenseCount FROM  Terminal WHERE LicenseID = ${licenseId}`)
                    .then(data => {
                        if (parseInt(data[0][0].TerminalLicenseCount) <= parseInt(licenseCount.replace(/^0+/, ''))) {
                            db.query('Insert_Terminal_TerminalDevices :TerminalName, :IpAddress, :MacAddress, :KeepAliveInterval, :BranchID, :LicenseID, :UserID',
                                { replacements: { TerminalName: terminalName, IpAddress: ipAddress, MacAddress: macAddress, KeepAliveInterval: parseInt(keepAliveInterval), BranchID: parseInt(branchId), LicenseID: parseInt(licenseId), UserID: parseInt(userId) } })
                                .then(data => {
                                    utils.doInfoLog(fileNameForLogging, req.url, 'terminal successfully added.')
                                    res.status(201).json({ response: { terminalId: data[0][0].Result } })
                                })
                                .catch(err => {
                                    utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                                    res.status(500).json({ response: errorResponse })
                                });
                        } else {
                            utils.doErrorLog(fileNameForLogging, req.url, 'license limit reached.')
                            const errorResponse = new Error('License limit reached. Please contact Intellekt admin.')
                            res.status(500).json({ response: errorResponse })
                        }
                    })
                    .catch(err => {
                        utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        res.status(500).json({ response: errorResponse })
                    })
            } else {
                utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG)
                res.status(500).json({ response: errorResponse })
            }
        }
    })
}

exports.generateLicense = (req, res) => {
    let licenseCount = req.params.numberOfLicenses
    licenseCount = _.padStart('', 4 - licenseCount.length, '0') + licenseCount
    const licenseKey = `${licenseCount}BAFKIOSKCSC`
    const ecryptedLicenseKey = utils.encrypt(licenseKey)
    if (ecryptedLicenseKey != '') {
        db.query(`INSERT INTO License(LicenseKey)  VALUES ('${ecryptedLicenseKey}')`)
            .then(data => {
                res.status(200).json({ response: ecryptedLicenseKey })
            })
            .catch(err => {
                utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                res.status(500).json({ response: errorResponse })
            });
    }


}

// ==============================================================================================
// ==============================================================================================

/**
 * ===============================================
 *  PUT REQUESTS METHODS
 * ===============================================
 **/

exports.updateTerminal = (req, res) => {
    utils.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    const { terminalName, ipAddress, macAddress, keepAliveInterval, branchId, licenseId, status, userId, statusChanged, reasonForStatus, terminalStatus } = req.body
    const terminalId = req.params.terminalId

    Joi.validate({ terminalName, ipAddress, macAddress, keepAliveInterval, branchId, licenseId, status, userId, statusChanged, reasonForStatus, terminalStatus },
        terminalValidation.updateTerminal, function (err, value) {
            if (err) {
                utils.doErrorLog(fileNameForLogging, req.url, 'invalid params : ', err)
                res.status(400).json(new Error(err.details[0].message, err.message, err))
            } else {
                db.query('Admin_UpdateTerminal :TerminalID, :TerminalName, :IpAddress, :MacAddress, :KeepAliveInterval, :BranchID, :LicenseID, :isAdminLock, :UserID, :StatusChanged, :NetworkStatus, :Reason',
                    {
                        replacements: {
                            TerminalID: terminalId,
                            TerminalName: terminalName,
                            IpAddress: ipAddress,
                            MacAddress: macAddress,
                            KeepAliveInterval: keepAliveInterval,
                            BranchID: branchId,
                            LicenseID: licenseId,
                            isAdminLock: status,
                            UserID: userId,
                            StatusChanged: statusChanged,
                            NetworkStatus: terminalStatus,
                            Reason: reasonForStatus
                        }
                    })
                    .then(data => {
                        utils.doInfoLog(fileNameForLogging, req.url, 'terminal successfully updated.')
                        res.status(200).json({ response: `'${terminalName}' has been successfully updated!` })
                    })
                    .catch(err => {
                        utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                        const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                        res.status(500).json({ response: errorResponse })
                    });
            }
        })
}

exports.updateTerminalCriticalStatus = (req, res) => {
    utils.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    const { userId, comments } = req.body
    const terminalId = req.params.terminalId

    Joi.validate({ userId, comments }, terminalValidation.updateTerminalCriticalStatus, function (err, value) {
        if (err) {
            utils.doErrorLog(fileNameForLogging, req.url, 'invalid params : ', err)
            res.status(400).json(new Error(err.details[0].message, err.message, err))
        } else {
            db.query('InsertTerminalCriticalLog :TerminalID, :UserID, :Comments',
                { replacements: { TerminalID: terminalId, UserID: userId, Comments: comments } })
                .then(data => {
                    utils.doInfoLog(fileNameForLogging, req.url, 'terminal critical status updated.')
                    res.status(200).json({ response: `Terminal status has been successfully updated!` })
                })
                .catch(err => {
                    utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    res.status(500).json({ response: errorResponse })
                });
        }
    })
}

exports.updateSerialNumberOfSpecificDeviceOfATerminal = (req, res) => {
    utils.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    const serialNumber = req.params.serialNumber
    const terminalDeviceId = req.params.terminalDeviceId

    Joi.validate({ serialNumber }, terminalValidation.updateSerialNumberOfSpecificDeviceOfATerminal, function (err, value) {
        if (err) {
            utils.doErrorLog(fileNameForLogging, req.url, 'invalid params : ', err)
            res.status(400).json(new Error(err.details[0].message, err.message, err))
        } else {
            db.query(`UPDATE TerminalDevices SET SerialNo = '${serialNumber}', LastUpdateDate=GETDATE() WHERE TerminalDeviceID = '${terminalDeviceId}'`)
                .then(data => {
                    utils.doInfoLog(fileNameForLogging, req.url, 'device serial number updated successfully.')
                    res.status(200).json({ response: `Device serial number has been successfully updated!` })
                })
                .catch(err => {
                    utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                    const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
                    res.status(500).json({ response: errorResponse })
                });
        }
    })
}

exports.updateSerialNumbersOfAllDevicesOfATerminal = (req, res) => {
    utils.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

    const terminalId = req.params.terminalId
    const [...serialNumbers] = req.body.serialNumbers
    if (serialNumbers.length > 0) {
        const serialNumbersInsertion = []
        for (let serialNumber of serialNumbers) {
            serialNumbersInsertion.push({
                TerminalID: terminalId, DeviceID: serialNumber.deviceId, SerialNo: serialNumber.serialNumber
            })
        }
        terminalDevicesModel(db, Sequelize)
            .destroy({ where: { TerminalID: terminalId } })
            .then(userRolesDestroyResponse => {
                terminalDevicesModel(db, Sequelize)
                    .bulkCreate(serialNumbersInsertion)
                    .then(userRolesInsertResponse => {
                        utils.doInfoLog(fileNameForLogging, req.url, 'serials numbers updated!')
                        res.status(200).json({ response: "Terminal successfully added !" })
                    })
            }).catch(err => {
                utils.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
                const errorResponse = new Error(appConfig.INVALID_PARAMS_ERROR_MSG, err.message, err)
                res.status(500).json({ response: errorResponse })
            })
    } else {
        utils.doErrorLog(fileNameForLogging, req.url, 'invalid params')
        const errorResponse = new Error(appConfig.INVALID_PARAMS_ERROR_MSG)
        res.status(400).json({ response: errorResponse })
    }
}


// ==============================================================================================




