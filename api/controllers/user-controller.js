const appConfig = require('../../app-config'),
  Joi = require('joi'),
  userValidation = require('../validations/validation-user'),
  md5 = require('md5'),
  userModel = require('../models/model-user'),
  Sequelize = require('sequelize'),
  userRoleModel = require('../models/model-user-roles'),
  db = require('../../db').getInstance(),
  randomstring = require("randomstring"),
  log = require('../utils'),
  fileNameForLogging = 'user-controller',
  utils = require('../utils')

import Error from '../response-classes/error'

// ==============================================================================================

/**
 * ===============================================
 *  GET REQUESTS METHODS
 * ===============================================
 **/

exports.getUsers = function (req, res) {
  db.query(`SELECT * FROM Users`)
    .then(results => {
      log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')

      let users = []
      for (let data of results[0]) {
        users.push({
          username: data.Username,
          fullname: data.FullName,
          email: data.Email,
          status: data.IsEnabled,
          userId: data.UserID
        })
      }
      res.status(200).json({ response: users })
    })
    .catch(err => {
      log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
      res.status(500).json({ response: errorResponse })
    })
}

exports.getUserById = function (req, res) {
  db.query(`  SELECT * FROM Users 
              INNER JOIN UserinRoles  ON UserinRoles.UserID=Users.UserID
              INNER JOIN Roles  ON UserinRoles.RoleID=Roles.RoleID
              WHERE Users.UserID='${req.params.userId}'`).spread((results, metadata) => {
      log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
      let roles = []
      for (let role of results) {
        roles.push({ roleName: role.RoleName, roleId: role.RoleID.toString() })
      }
      const user = {
        username: results[0].Username,
        fullname: results[0].FullName,
        email: results[0].Email,
        mobile: results[0].Mobile,
        userStatus: results[0].IsEnabled,
        roles
      }
      res.status(200).json({ response: user })
    })
    .catch(err => {
      log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
      const errorResponse = new Error('Sorry unable to proccess. Please try again!', err.message, err)
      res.status(500).json({ response: errorResponse })
    });
}

exports.getRoles = function (req, res) {
  db.query(`SELECT * FROM Roles`)
    .then(data => {
      log.doInfoLog(fileNameForLogging, req.url, 'data successfully fetched.')
      let roles = []
      for (let role of data[0]) {
        roles.push({
          roleId: role.RoleID,
          roleName: role.RoleName
        })
      }
      res.status(200).json({ response: roles })
    })
    .catch(err => {
      log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
      const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
      res.status(500).json({ response: errorResponse })
    });
}


// ==============================================================================================
// ==============================================================================================

/**
 * ===============================================
 *  POST REQUESTS METHODS
 * ===============================================
 **/


exports.addUser = function (req, res) {

  log.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

  const { username, fullname, email, mobile } = req.body
  const [...roles] = req.body.roles
  const password = randomstring.generate({
    length: 10,
    charset: 'alphanumeric'
  })
  // const text = `Password : ${password}`

  Joi.validate({ username, fullname, email, mobile, roles }, userValidation.addUser, function (err, value) {
    if (err) {
      log.doErrorLog(fileNameForLogging, req.url, 'invalid params : ', err)
      res.status(400).json(new Error(err.details[0].message, err.message, err))
    } else {
      const user = { Username: username, FullName: fullname, Password: md5(password), Email: email, Mobile: mobile }
      userModel(db, Sequelize).create(user)
        .then(task => {
          const userRolesInsertion = []
          for (let value of roles) {
            userRolesInsertion.push({ RoleID: value, UserID: task.dataValues.UserID })
          }
          userRoleModel(db, Sequelize)
            .bulkCreate(userRolesInsertion)
            .then(tasks => {
              utils.sendEmail(password, email, err => {
                if (err) {
                  log.doErrorLog(fileNameForLogging, req.url, 'email sent failed : ', err)
                } else {
                  log.doInfoLog(fileNameForLogging, req.url, 'email sent success')
                }
              })
              log.doInfoLog(fileNameForLogging, req.url, 'user successfully created. ')
              res.status(201).json({ response: "User successfully created !" })
            })
        })
        .catch(err => {
          log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
          let errorResponse
          if (err.name === 'SequelizeUniqueConstraintError') {
            errorResponse = new Error(appConfig.DUPLICATE_DATA_ERROR_MSG, err.message, err)
          } else {
            errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
          }
          res.status(500).json({ response: errorResponse })
        })
    }
  })
}


// ==============================================================================================
// ==============================================================================================

/**
 * ===============================================
 *  PUT REQUESTS METHODS
 * ===============================================
 **/

exports.updateUser = function (req, res) {

  log.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

  const { username, fullname, email, mobile, isEnabled } = req.body
  const userId = req.params.userId
  const [...roles] = req.body.roles
  Joi.validate({ username, fullname, email, mobile, isEnabled, roles }, userValidation.updateUser, function (err, value) {
    if (err) {
      log.doErrorLog(fileNameForLogging, req.url, 'invalid params : ', err)
      const errorResponse = new Error(err.details[0].message, err.message, err);
      res.status(400).json({ response: errorResponse })
    } else {
      db.query(`  UPDATE Users SET
                  Username = '${username}',
                  FullName = '${fullname}', 
                  Email = '${email}', 
                  Mobile = '${mobile}',
                  IsEnabled = '${isEnabled}',              
                  UpdatedDate = GETDATE()
                  WHERE UserID = '${userId}' `)
        .then(data => {
          userRoleModel(db, Sequelize)
            .destroy({ where: { UserID: userId } })
            .then(userRolesDestroyResponse => {
              const userRolesInsertion = []
              for (let value of roles) {
                userRolesInsertion.push({ RoleID: value, UserID: userId })
              }
              userRoleModel(db, Sequelize)
                .bulkCreate(userRolesInsertion)
                .then(userRolesInsertResponse => {
                  log.doInfoLog(fileNameForLogging, req.url, 'User successfully updated!')
                  res.status(200).json({ response: "User successfully updated!" })
                })
            })
            .catch(error => {
              log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', error)
              res.status(500).json({ response: new Error(appConfig.GENERIC_ERROR_MSG, error.message, error) })
            })
        })
        .catch(error => {
          log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', error)
          res.status(500).json({ response: new Error(appConfig.GENERIC_ERROR_MSG, error.message, error) })
        })
      // const user = { Username: username, FullName: fullname, Password: md5(appConfig.USER_DEFAULT_PASSWORD), Email: email, Mobile: mobile }
      // userModel(db, Sequelize)
      //   .update(user, { where: { UserID: req.params.userId } })
      //   .then(usersResponse => {
      //     userRoleModel(db, Sequelize)
      //       .destroy({ where: { UserID: req.params.userId } })
      //       .then(userRolesDestroyResponse => {
      //         const userRolesInsertion = []
      //         for (let value of roles) {
      //           userRolesInsertion.push({ RoleID: value, UserID: req.params.userId })
      //         }
      //         userRoleModel(db, Sequelize)
      //           .bulkCreate(userRolesInsertion)
      //           .then(userRolesInsertResponse => {
      //             res.status(201).json({response: "User successfully updated!"})
      //           })
      //       })
      //   })
      //   .catch(err => {
      //     res.status(500).json({ response: new Error(appConfig.GENERIC_ERROR_MSG, err.message, err) })
      //   })
    }
  })
}

exports.updatePassword = function (req, res) {

  log.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)

  const { password } = req.body
  const userId = req.params.userId

  Joi.validate({ password }, userValidation.updatePassword, function (err, value) {
    if (err) {
      log.doErrorLog(fileNameForLogging, req.url, 'invalid params : ', err)
      const errorResponse = new Error(err.details[0].message, err.message, err);
      res.status(400).json({ Error: errorResponse })
    } else {
      db.query(`UPDATE Users SET Password = '${md5(password)}', LastPwdChangeDate = GETDATE() , UpdatedDate = GETDATE() WHERE UserID = '${userId}'`)
        .then(results => {
          log.doInfoLog(fileNameForLogging, req.url, 'Password has been successfully updated.')
          res.status(200).json({ response: `Password has been successfully updated.` })
        })
        .catch(err => {
          log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
          res.status(500).json({ response: errorResponse })
        })
    }
  })
}

exports.resetPassword = function (req, res) {
  log.doInfoLog(fileNameForLogging, req.url, 'request body : ', req.body)
  const { email } = req.body
  const userId = req.params.userId
  let password = randomstring.generate({
    length: 10,
    charset: 'alphanumeric'
  })
  // const subject = 'INTELLEKT: USER PASSWORD RESET'
  // const text = `Password : ${password}`

  Joi.validate({ email }, userValidation.resetPassword, function (err, value) {
    if (err) {
      log.doErrorLog(fileNameForLogging, req.url, 'invalid params : ', err)
      res.status(400).json(new Error(err.details[0].message, err.message, err))
    } else {
      utils.sendEmail(password, email, err => {
        if (err) {
          log.doFatalLog(fileNameForLogging, req.url, 'exception in email send : ', err)
          const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, '', err)
          res.status(500).json({ response: errorResponse })
        } else {
          db.query(`UPDATE Users SET Password = '${md5(password)}', LastPwdChangeDate = GETDATE() , UpdatedDate = GETDATE()   WHERE UserID = '${userId}'`)
            .then(results => {
              log.doInfoLog(fileNameForLogging, req.url, 'password reset success and email sent.')
              res.status(200).json({ response: `Password has been successfully reset.` })
            })
            .catch(err => {
              log.doFatalLog(fileNameForLogging, req.url, 'exception in db : ', err)
              const errorResponse = new Error(appConfig.GENERIC_ERROR_MSG, err.message, err)
              res.status(500).json({ response: errorResponse })
            })
        }
      })
    }
  })
}

