/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('UserinRoles', {
      UserRoleId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      RoleID: {
        type: DataTypes.INTEGER,
        allowNull: true
      },
      UserID: {
        type: DataTypes.INTEGER,
        allowNull: true
      }
    }, {
      tableName: 'UserinRoles',
      timestamps: false
    });
  };
  