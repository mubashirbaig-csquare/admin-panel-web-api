/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Roles', {
      RoleID: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      RoleName: {
        type: DataTypes.STRING,
        allowNull: true
      }
    }, {
      tableName: 'Roles'
    });
  };
  