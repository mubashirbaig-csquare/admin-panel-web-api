/* jshint indent: 2 */
const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
    return sequelize.define('TerminalDevices', {
      TerminalDeviceID: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      TerminalID: {
        type: DataTypes.INTEGER,
        allowNull: true
      },
      DeviceID: {
        type: DataTypes.STRING,
        allowNull: true
      },
      SerialNo: {
        type: DataTypes.STRING,
        allowNull: true
      },
      IsActive: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: '1'
      },
      LastUpdateDate: {
        type: Sequelize.DATE
      }
    }, {
      tableName: 'TerminalDevices',
      timestamps: false
    });
  };
  