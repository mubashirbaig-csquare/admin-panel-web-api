/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Users', {
      UserID: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      Username: {
        type: DataTypes.STRING,
        allowNull: true
      },
      FullName: {
        type: DataTypes.STRING,
        allowNull: true
      },
      Password: {
        type: DataTypes.STRING,
        allowNull: true
      },
      IsEnabled: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: '1'
      },
      CreatedDate: {
        type: DataTypes.DATE,
        allowNull: true
      },
      UpdatedDate: {
        type: DataTypes.DATE,
        allowNull: true
      },
      LastLoginDate: {
        type: DataTypes.DATE,
        allowNull: true
      },
      Email: {
        type: DataTypes.STRING,
        allowNull: true
      },
      Mobile: {
        type: DataTypes.STRING,
        allowNull: true
      },
      LastPwdChangeDate: {
        type: DataTypes.DATE,
        allowNull: true
      }
    }, {
      timestamps: false,
      tableName: 'Users'
    });
  };
  