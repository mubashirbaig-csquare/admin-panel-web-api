/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Terminal', {
      TerminalID: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      TerminalName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      IpAddress: {
        type: DataTypes.STRING,
        allowNull: false
      },
      MacAddress: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: ''
      },
      TerminaHostName: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: ''
      },
      KeepAliveInterval: {
        type: DataTypes.INTEGER,
        allowNull: true
      },
      TerminalConfig: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      BranchID: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      LicenseID: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: '((0))'
      },
      Status: {
        type: DataTypes.BOOLEAN,
        allowNull: true
      },
      CreatedDate: {
        type: DataTypes.DATE,
        allowNull: true
      },
      UpdateDate: {
        type: DataTypes.DATE,
        allowNull: true
      },
      UserID: {
        type: DataTypes.INTEGER,
        allowNull: true
      }
    }, {
      timestamps: false,
      tableName: 'Terminal'
    });
  };
  