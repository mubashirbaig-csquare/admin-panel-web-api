/**
 * =====================================================
 *  App configurations
 * =====================================================
*/
process.env.PORT = 3000
process.env.DEVELOPMENT = "true"
const APP_TIMEOUT= 10000 // 10 seconds


/**
 * =====================================================
 *  JWT OPTIONS
 * =====================================================
*/

const JWT_SECRET_KEY = "?&qfI]r&>CVW8<ff[;zIoCJ*zxN]-.bA4}DTHGAJWkHzb!Oa(U@ObN3O*VnL)yP"
const JWT_EXPIRY_TIME = "24h"

/**
 * =====================================================
 *  USER DEFAULT PASSWORD
 * =====================================================
*/

const USER_DEFAULT_PASSWORD = "Passw0rd12"


/**
 * =====================================================
 *  EMAIL SETTINGS
 * =====================================================
*/

const EMAIL_HOST = 'mail.csquare.co'
const EMAIL_PORT = 26
const EMAIL_SECURE = false
const EMAIL_USERNAME = 'mubashir.baig@csquare.co'
const EMAIL_PASSWORD = 'Mub@sh!r786#'
const EMAIL_FROM_ACCOUNT = '"Info Intellekt" <info@intellekt.com>'
const EMAIL_SUBJECT = 'Welcome to KIOSK Admin Panel'



/**
 * =====================================================
 *  User Response Messages
 * =====================================================
*/

const GENERIC_ERROR_MSG = "Sorry unable to process. Please try again !"
const DUPLICATE_DATA_ERROR_MSG = "Data already exists!"
const INVALID_PARAMS_ERROR_MSG = "Mandatory request parameters missing !"
const NO_DATA_ERROR_MSG = "Currently there is no data available !"

/**
 * =====================================================
 *  DB CONFIG PROPERTIES
 * =====================================================
*/
function getDbConfig() {
    if (process.env.DEVELOPMENT === "true") {
        // return {
        //     host: "localhost",
        //     database: "KIOSKAdmin",
        //     username: "sa",
        //     password: "password"
        // }
         return {
            host: "192.168.1.217",
            database: "KIOSKAdmin",
            username: "sa",
            password: "P@ssw0rd"
        }
    } else {
         return {
            host: "172.24.24.152",
            database: "KIOSKAdmin",
            username: "sa",
            password: "sa123"
        }
    }
}


module.exports = {
    getDbConfig,
    JWT_SECRET_KEY,
    USER_DEFAULT_PASSWORD,
    GENERIC_ERROR_MSG,
    JWT_EXPIRY_TIME,
    APP_TIMEOUT,
    DUPLICATE_DATA_ERROR_MSG,
    INVALID_PARAMS_ERROR_MSG,
    NO_DATA_ERROR_MSG,
    EMAIL_HOST,
    EMAIL_PORT,
    EMAIL_SECURE,
    EMAIL_USERNAME,
    EMAIL_PASSWORD,
    EMAIL_FROM_ACCOUNT,
    EMAIL_SUBJECT
}