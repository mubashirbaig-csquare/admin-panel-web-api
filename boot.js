const bodyParser = require('body-parser'),
    timeout = require('express-timeout-handler'),
    routes = require('./api/routes/routes-index'),
    // shutdownHandler = require('shutdown-handler'),
    appConfig = require('./app-config'),
    passport = require("passport"),
    passportJWT = require("passport-jwt"),
    ExtractJwt = passportJWT.ExtractJwt,
    JwtStrategy = passportJWT.Strategy,
    cors = require('cors'),
    morgan = require('morgan'),
    utils = require('./api/utils'),
    db = require('./db').getInstance(),
    fileNameForLogging = 'boot',
    port = process.env.PORT || 3001

import Error from './api/response-classes/error'

module.exports = function (app) {
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(bodyParser.json())
    app.use(cors())
    // app.use(morgan('dev'))
    app.use(morgan('dev', {
        skip: function (req, res) { return res.statusCode < 400 }
    }))
    app.use(timeout.handler(options))
    settingAuthStrategy(app)
    routes(app, passport)
    configureDb(app)
    // shutdownHandler.on('exit', function () {
    //     console.log("Shutdown...")
    // })
}

var options = {
    timeout: appConfig.APP_TIMEOUT,
    onTimeout: function (req, res) {
        utils.doErrorLog(fileNameForLogging, req.url, 'request timeout.')
        res.status(503).json({ response: new Error(appConfig.GENERIC_ERROR_MSG, "request timeout.") })
    },
}

function configureDb(app) {
    db.authenticate()
        .then(() => {
            app.listen(port)
            console.log('Admin Panel RESTful API server started on: ' + port)
        })
        .catch(err => {
            console.error('Unable to connect to the database:');
            console.log("Shutdown...")
            process.exit()
        });
}

function settingAuthStrategy(app) {
    let jwtOptions = {}
    jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    jwtOptions.secretOrKey = appConfig.JWT_SECRET_KEY
    let strategy = new JwtStrategy(jwtOptions, function (jwt_payload, next) {
        next(null, true)
    })
    passport.use(strategy)
    app.use(passport.initialize())
}


